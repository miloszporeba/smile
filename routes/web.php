<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::group(['middleware' => 'web'], function() {
    Route::auth();
    Route::get('/home', 'MailingListsController@index');
    Route::get('/lists/', 'MailingListsController@index')->name('lists');
    Route::get('/lists/create/', 'MailingListsController@create')->name('listsadd');
    Route::post('/lists/create/', 'MailingListsController@store')->name('listcreate');
    Route::get('/lists/edit/{list}', 'MailingListsController@edit')->name('listedit');
    Route::post('/lists/update/', 'MailingListsController@update');
    Route::get('/lists/destroy/{list}', 'MailingListsController@destroy')->name('listdestroy');
});
