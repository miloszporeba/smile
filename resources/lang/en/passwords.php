<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are the default lines which match reasons
    | that are given by the password broker for a password update attempt
    | has failed, such as for an invalid token or invalid new password.
    |
    */

    'password' => 'Passwords must be at least six characters and match the confirmation.',
    'password' => 'Hasło musi mieć conajmniej 6 znaków i pasować do potwierdzenia',
    'reset' => 'Twoje hasło zostało zresetowane',
    'sent' => 'Link resetujący hasło został do Ciebie wysłany',
    'token' => 'This password reset token is invalid.',
    'user' => "Użytkownik z takim adresem email nie istnieje",

];
