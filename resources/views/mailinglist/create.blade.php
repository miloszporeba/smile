@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard / <span>Nowa lista mailingowa</span></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                                {{ Form::open(array('action' => 'MailingListsController@store')) }}
                                    {{ Form::label('name', 'Nazwa Twojej listy') }}
                                    {{ Form::text('name', 'Lista klientów', array('class' => 'form-control')) }} <br>
                                    {{ Form::submit('Stwórz listę!',  array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
