@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard / <span>Edytuj listę</span></div>

                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-8 col-md-offset-2">
                                {{ Form::open(array('action' => 'MailingListsController@update')) }}
                                    {{ Form::label('name', 'Nazwa Twojej listy') }}
                                    {{ Form::text('name', $list['name'], array('class' => 'form-control')) }} <br>
                                    {{ Form::hidden('id', $list['id']) }}
                                    {{ Form::hidden('user_id', $list['user_id']) }}
                                    {{ Form::submit('Zaktualizuj listę!',  array('class' => 'btn btn-primary')) }}
                                {{ Form::close() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
