@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-10 col-md-offset-1">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard / <span>Moje listy mailingowe</span></div>

                <div class="panel-body">
                     @if (Session::has('msg'))
                     	<div class="alert alert-success alert-dismissible" role="alert">
                          <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                          <strong>{{ Session::get("msg") }}</strong>.
                        </div>
                     @endif

                     <table class="table table-striped">
                         <thead>
                             <tr>
                                 <th>#</th>
                                 <th>Nazwa</th>
                                 <th>Ilość osób</th>
                                 <th></th>
                             </tr>
                         </thead>
                         <tbody>
                     		@foreach($lists as $list)
								<tr>
								 <th scope="row">{{ $loop->iteration }}</th>
								 <td>{{ $list['name'] }}</td>
								 <td>1</td>
								 <td>
                                    <div class="pull-right">
                                        <a title="Edytuj" href="{{ route('listedit', ['id' => $list['id']]) }}" class="btn btn-default"><i class="fa fa-edit"></i></a>
                                        <a title="Usuń" href="{{ route('listdestroy', ['id' => $list['id']]) }}" class="btn btn-default"><i class="fa fa-trash"></i></a>
                                    </div>
                                 </td>
								</tr>
                     	    @endforeach
                         </tbody>
                     </table>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
