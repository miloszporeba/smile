<?php

namespace App\Http\Controllers;

use App\MailingLists;
use Illuminate\Http\Request;
use Auth;
use DB;
use View;


class MailingListsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (Auth::check()) {
            $lists = MailingLists::where('user_id', Auth::id())
                ->orderBy('created_at', 'desc')
                ->get();
            return view('mailinglist.index')->with('lists', $lists);
        }

        return redirect()->route('login'); 
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if (Auth::check()) {
            return view('mailinglist.create');
        }

        return redirect()->route('login'); 
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if (Auth::check()) {
            $mailinglist = MailingLists::create([
                'name' => $request->input('name'),
                'user_id' => Auth::user()->id
            ]);


            if ($mailinglist['id']) {
               session()->flash('msg', 'Lista stworzona poprawnie');
               return redirect()->route('lists');
            }
        }

        return redirect()->route('login'); 
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\MailingLists  $mailingLists
     * @return \Illuminate\Http\Response
     */
    public function show(MailingLists $mailingLists)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\MailingLists  $mailingLists
     * @return \Illuminate\Http\Response
     */
    public function edit(MailingLists $list)
    {
        if (Auth::check()) {
            if (Auth::user()->id === $list->user_id) {
                return view('mailinglist.edit')->with('list', $list);
            }

            Auth::logout(); 
        }

        return redirect()->route('login'); 
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\MailingLists  $mailingLists
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        if (Auth::check()) {
            if (Auth::user()->id == $request->user_id) {
                DB::table('mailing_lists')
                    ->where('id', $request->id)
                    ->update(['name' => $request->name]);

                session()->flash('msg', 'Lista zmieniona poprawnie');
                return redirect()->route('lists');
            }

            Auth::logout(); 
            
        }

        return redirect()->route('login'); 
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\MailingLists  $mailingLists
     * @return \Illuminate\Http\Response
     */
    public function destroy(MailingLists $list)
    {
        if (Auth::check()) {
            if (Auth::user()->id === $list->user_id) {
                DB::table('mailing_lists')->where('id', $list->id)->delete();

                session()->flash('msg', 'Lista została usunięta');
                return redirect()->route('lists');
            }

            Auth::logout(); 
            
        }

        return redirect()->route('login'); 
    }
}
